FROM ubuntu:latest as build

RUN apt update && apt install -y libpcre3 libpcre3-dev perl zlib1g zlib1g-dev libssl-dev gcc g++ make wget

ARG VERSION_NGINX
ENV VERSION_NGINX 1.23.3

WORKDIR /tmp
RUN set -x && \
    wget -q http://nginx.org/download/nginx-${VERSION_NGINX}.tar.gz && \
    tar -xvf nginx-${VERSION_NGINX}.tar.gz

WORKDIR /tmp/nginx-${VERSION_NGINX}

RUN ./configure                                                                     \
        --with-ld-opt="-static"                                                     \
        --with-http_sub_module                                                  &&  \
    make install                                                                &&  \
    strip /usr/local/nginx/sbin/nginx

FROM scratch

COPY --from=build /usr/local/nginx /usr/local/nginx
COPY --from=build /etc/passwd /etc/group /etc/
EXPOSE 80

ENTRYPOINT ["/usr/local/nginx/sbin/nginx"]
CMD ["-g", "daemon off;"]